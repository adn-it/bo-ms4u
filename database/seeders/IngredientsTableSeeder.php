<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name'=>'açúcar'],
            ['name'=>'açúcar invertido'],
            ['name'=>'água'],
            ['name'=>'AMÊNDOAS'],
            ['name'=>'amido de TRIGO'],
            ['name'=>'ANT antioxidante E300'],
            ['name'=>'aroma'],
            ['name'=>'aroma natural'],
            ['name'=>'aroma natural de baunilha'],
            ['name'=>'aroma natural de limão'],
            ['name'=>'AVELÃS'],
            ['name'=>'cacau magro em pó '],
            ['name'=>'CO E100'],
            ['name'=>'CO E120'],
            ['name'=>'CO E132'],
            ['name'=>'CO E141'],
            ['name'=>'CO E160a'],
            ['name'=>'CO E160c'],
            ['name'=>'CO E171'],
            ['name'=>'CO E172'],
            ['name'=>'CONC batata doce'],
            ['name'=>'CONC cártamo'],
            ['name'=>'CONC cenoura'],
            ['name'=>'CONC cereja'],
            ['name'=>'CONC extrato de beterraba'],
            ['name'=>'CONC framboesa'],
            ['name'=>'CONC limão'],
            ['name'=>'CONC maçã'],
            ['name'=>'CONC rabanete'],
            ['name'=>'CONC spirulina'],
            ['name'=>'CONSERV conservante E202'],
            ['name'=>'dextrose'],
            ['name'=>'EM E471'],
            ['name'=>'EM LEC SOJA'],
            ['name'=>'EM lecitina'],
            ['name'=>'especiarias'],
            ['name'=>'ESPESS espessante E413'],
            ['name'=>'ESPESS espessante E415'],
            ['name'=>'ESPESS espessante E440'],
            ['name'=>'EST estabilizador (E420)'],
            ['name'=>'EST estabilizador (E422)'],
            ['name'=>'EST estabilizador (E440)'],
            ['name'=>'Farinha de arroz'],
            ['name'=>'Farinha de TRIGO'],
            ['name'=>'framboesa'],
            ['name'=>'Framboise 50% Vol'],
            ['name'=>'GLÚTEN de TRIGO'],
            ['name'=>'gordura de LEITE'],
            ['name'=>'gordura de LEITE anidra'],
            ['name'=>'GV coco'],
            ['name'=>'GV colza'],
            ['name'=>'GV girassol'],
            ['name'=>'GV palma'],
            ['name'=>'LACTOSE'],
            ['name'=>'LEITE gordo'],
            ['name'=>'LEITE gordo em pó'],
            ['name'=>'LEITE magro em pó'],
            ['name'=>'LEV levedante E500'],
            ['name'=>'LEV levedante E501'],
            ['name'=>'LEV levedante E503'],
            ['name'=>'malte de CEVADA'],
            ['name'=>'malte de TRIGO'],
            ['name'=>'manteiga (LEITE)'],
            ['name'=>'manteiga concentrada (LEITE)'],
            ['name'=>'manteiga de cacau'],
            ['name'=>'morango'],
            ['name'=>'nata (LEITE)'],
            ['name'=>'NOZES PÉCAN'],
            ['name'=>'OV coco'],
            ['name'=>'OV girassol'],
            ['name'=>'OV palma'],
            ['name'=>'pasta de cacau'],
            ['name'=>'PISTÁCHIOS'],
            ['name'=>'RA reguladores de acidez E330'],
            ['name'=>'RA reguladores de acidez E331'],
            ['name'=>'REVEST agente de revestimento E904'],
            ['name'=>'REVEST agente de revestimento E901'],
            ['name'=>'sal'],
            ['name'=>'soro Leite em pó'],
            ['name'=>'xarope de açúcar invertido'],
            ['name'=>'xarope de glucose']
        ])->each(function ($i) {
            Ingredient::create($i);
        });
    }
}
