<?php

namespace Database\Seeders;

use App\Models\IGroup;
use Illuminate\Database\Seeder;

class IgroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name'=>'concentrados'],
            ['name'=>'gorduras vegetais'],
            ['name'=>'corantes'],
            ['name'=>'levedantes'],
            ['name'=>'manteiga'],
            ['name'=>'espessante'],
            ['name'=>'manteiga concentrada'],
            ['name'=>'agente de revestimento'],
            ['name'=>'regulador de acidez'],
        ])->each(function ($i) {
            IGroup::create($i);
        });
    }
}


