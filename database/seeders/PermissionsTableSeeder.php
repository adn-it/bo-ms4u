<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'ingredient_create',
            ],
            [
                'id'    => 18,
                'title' => 'ingredient_edit',
            ],
            [
                'id'    => 19,
                'title' => 'ingredient_show',
            ],
            [
                'id'    => 20,
                'title' => 'ingredient_delete',
            ],
            [
                'id'    => 21,
                'title' => 'ingredient_access',
            ],
            [
                'id'    => 22,
                'title' => 'recipe_create',
            ],
            [
                'id'    => 23,
                'title' => 'recipe_edit',
            ],
            [
                'id'    => 24,
                'title' => 'recipe_show',
            ],
            [
                'id'    => 25,
                'title' => 'recipe_delete',
            ],
            [
                'id'    => 26,
                'title' => 'recipe_access',
            ],
            [
                'id'    => 27,
                'title' => 'profile_password_edit',
            ],
            [
                'id'    => 28,
                'title' => 'nutrient_create',
            ],
            [
                'id'    => 29,
                'title' => 'nutrient_edit',
            ],
            [
                'id'    => 30,
                'title' => 'nutrient_show',
            ],
            [
                'id'    => 31,
                'title' => 'nutrient_delete',
            ],
            [
                'id'    => 32,
                'title' => 'nutrient_access',
            ],
            [
                'id'    => 33,
                'title' => 'pack_create',
            ],
            [
                'id'    => 34,
                'title' => 'pack_edit',
            ],
            [
                'id'    => 35,
                'title' => 'pack_show',
            ],
            [
                'id'    => 36,
                'title' => 'pack_delete',
            ],
            [
                'id'    => 37,
                'title' => 'pack_access',
            ],
            [
                'id'    => 38,
                'title' => 'igroup_access',
            ],
            [
                'id'    => 39,
                'title' => 'igroup_create',
            ],
            [
                'id'    => 40,
                'title' => 'igroup_edit',
            ],
            [
                'id'    => 41,
                'title' => 'igroup_show',
            ],
            [
                'id'    => 42,
                'title' => 'igroup_delete',
            ],
            [
                'id'    => 43,
                'title' => 'igroup_access',
            ],
            [
                'id'    => 44,
                'title' => 'ingredient_management_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
