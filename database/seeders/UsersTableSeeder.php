<?php


namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'name'           => 'Admin',
                'email'          => 'antonio.sequeira@adanora.pt',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ],
            [
                'name'           => 'User',
                'email'          => 'info@mysweets4u.com',
                'password'       => bcrypt('info2021'),
                'remember_token' => null,
            ],
            [
                'name'           => 'User',
                'email'          => 'joao.gomes@sweets4u.pt',
                'password'       => bcrypt('joaobo2021'),
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
