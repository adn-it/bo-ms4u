<?php

namespace Database\Seeders;

use App\Models\Nutrient;
use Illuminate\Database\Seeder;

class NutrientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name'=>'Energia kcal'],
            ['name'=>'Energia kj'],
            ['name'=>'Lípidos'],
            ['name'=>'Dos quais saturados'],
            ['name'=>'Hidratos de Carbono'],
            ['name'=>'Dos quais açúcares'],
            ['name'=>'Proteínas'],
            ['name'=>'Fibra'],
            ['name'=>'Sal']
        ])->each(function ($i) {
            Nutrient::create($i);
        });
    }
}
