<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNutrientRecipePivotTable extends Migration
{
    public function up()
    {
        Schema::create('nutrient_recipe', function (Blueprint $table) {
            $table->unsignedBigInteger('recipe_id');
            $table->foreign('recipe_id', 'recipe_id_fk_2608792')->references('id')->on('recipes')->onDelete('cascade');
            $table->unsignedBigInteger('nutrient_id');
            $table->foreign('nutrient_id', 'nutrient_id_fk_2608793')->references('id')->on('nutrients')->onDelete('cascade');
            $table->decimal('amount',8,3)->nullable()->default(0.00);
        });
    }
}
