<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientPackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_pack', function (Blueprint $table) {
            $table->unsignedBigInteger('pack_id');
            $table->foreign('pack_id', 'pack_id_fk_2608792')->references('id')->on('packs')->onDelete('cascade');
            $table->unsignedBigInteger('ingredient_id');
            $table->foreign('ingredient_id', 'ingredient_id_fk_2608792')->references('id')->on('ingredients')->onDelete('cascade');
            $table->decimal('amount',8,3)->nullable()->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_pack');
    }
}



