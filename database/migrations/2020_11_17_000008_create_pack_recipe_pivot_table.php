<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackRecipePivotTable extends Migration
{
    public function up()
    {
        Schema::create('pack_recipe', function (Blueprint $table) {
            $table->unsignedBigInteger('pack_id');
            $table->foreign('pack_id', 'pack_id_fk_2608799')->references('id')->on('packs')->onDelete('cascade');
            $table->unsignedBigInteger('recipe_id');
            $table->foreign('recipe_id', 'recipe_id_fk_2608799')->references('id')->on('recipes')->onDelete('cascade');
            $table->decimal('amount',8,3)->nullable()->default(0.00);
        });
    }
}
