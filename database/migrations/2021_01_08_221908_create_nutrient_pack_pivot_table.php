<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNutrientPackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutrient_pack', function (Blueprint $table) {
            $table->unsignedBigInteger('pack_id');
            $table->foreign('pack_id', 'pack_id_fk_2608793')->references('id')->on('packs')->onDelete('cascade');
            $table->unsignedBigInteger('nutrient_id');
            $table->foreign('nutrient_id', 'nutrient_id_fk_2608794')->references('id')->on('nutrients')->onDelete('cascade');
            $table->decimal('amount',8,3)->nullable()->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nutrient_pack');
    }
}
