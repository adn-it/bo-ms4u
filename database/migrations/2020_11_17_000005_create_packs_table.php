<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacksTable extends Migration
{
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('fullname');
            $table->string('thumbnail')->nullable();
            $table->decimal('weight',8,3)->nullable()->default(0.00);
            $table->decimal('weight_real',8,3)->nullable()->default(0.00);
            $table->text('nutritional_info')->nullable();
            $table->text('ingredients_info')->nullable();
            $table->text('allergenic_info')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
