<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Ingredients
    Route::delete('ingredients/destroy', 'IngredientsController@massDestroy')->name('ingredients.massDestroy');
    Route::resource('ingredients', 'IngredientsController');

    // Ingredient Groups
    Route::delete('igroups/destroy', 'IGroupsController@massDestroy')->name('igroups.massDestroy');
    Route::resource('igroups', 'IGroupsController');

    // Nutrients
    Route::delete('nutrients/destroy', 'NutrientsController@massDestroy')->name('nutrients.massDestroy');
    Route::resource('nutrients', 'NutrientsController');

    // Recipes
    Route::delete('recipes/destroy', 'RecipesController@massDestroy')->name('recipes.massDestroy');
    Route::get('recipes/{recipe}/editnutrients', 'RecipesController@editnutrients')->name('recipes.editnutrients');
    Route::get('recipes/{recipe}/editingredients', 'RecipesController@editingredients')->name('recipes.editingredients');
//    Route::post('recipes/updatenutrients', 'RecipesController@updatenutrients')->name('recipes.updatenutrients');
    Route::resource('recipes', 'RecipesController');


    // Packs
    Route::delete('packs/destroy', 'PacksController@massDestroy')->name('packs.massDestroy');
    Route::resource('packs', 'PacksController');
});

Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
