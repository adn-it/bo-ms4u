<?php

namespace App\Http\Requests;

use App\Models\IGroup;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreIGroupRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('igroup_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'ingredients.*' => [
                'integer',
            ],
            'ingredients'   => [
                'array',
            ],
        ];
    }
}
