<?php

namespace App\Http\Requests;

use App\Models\Pack;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdatePackRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('pack_edit');
    }

    public function rules()
    {
        return [
            'name'          => [
                'string',
                'required',
            ],
            'fullname'      => [
                'string',
                'required',
            ],
            'ingredients.*' => [
                'regex:/^[+]?\d*(\.\d+)?$/',
            ],
            'ingredients'   => [
                'array',
            ],
            'nutrients'   => [
                'array',
            ],
            'nutrients.*' => [
                'regex:/^[+]?\d*(\.\d+)?$/',
            ],
            'recipes.*' => [
                'numeric',
            ],
            'recipes'   => [
                'required',
                'array',
            ],
        ];
    }
}
