<?php

namespace App\Http\Requests;

use App\Models\GroupIngredient;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGroupIngredientRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('groupingredient_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
