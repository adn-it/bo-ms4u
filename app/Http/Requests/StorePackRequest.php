<?php

namespace App\Http\Requests;

use App\Models\Pack;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePackRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('pack_create');
    }

    public function rules()
    {
        return [
            'name'          => [
                'string',
                'required',
            ],
            'fullname'      => [
                'string',
                'required',
            ],
            'recipes.*' => [
                'string',
            ],
            'recipes'   => [
                'required',
                'array',
            ],
            'ingredients.*' => [
                'string',
            ],
            'ingredients'   => [
                'array',
            ],
            'nutrients.*' => [
                'string',
            ],
            'nutrients'   => [
                'array',
            ],
        ];
    }
}
