<?php

namespace App\Http\Requests;

use App\Models\Recipe;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRecipeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('recipe_edit');
    }

    public function rules()
    {
        return [
            'name'          => [
                'string',
                'required',
            ],
            'fullname'      => [
                'string',
                'required',
            ],
            'weight'          => [
                'regex:/^[+]?\d*(\.\d+)?$/',
            ],
            'weight_real'     => [
                'regex:/^[+]?\d*(\.\d+)?$/',
            ],
            'ingredients.*' => [
                'string',
            ],
            'ingredients'   => [
                'array',
            ],
            'nutrients.*' => [
                'string',
            ],
            'nutrients'   => [
                'array',
            ],
            'technical_sheet_date'   => [
                'date',
            ],
        ];
    }
}
