<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyNutrientRequest;
use App\Http\Requests\StoreNutrientRequest;
use App\Http\Requests\UpdateNutrientRequest;
use App\Models\Nutrient;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NutrientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('nutrient_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nutrients = Nutrient::all();

        return view('admin.nutrients.index', compact('nutrients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('nutrient_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nutrients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNutrientRequest $request)
    {
        $nutrient = Nutrient::create($request->all());

        return redirect()->route('admin.nutrients.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nutrient  $nutrient
     * @return \Illuminate\Http\Response
     */
    public function edit(Nutrient $nutrient)
    {
        abort_if(Gate::denies('nutrient_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nutrients.edit', compact('nutrient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Nutrient  $nutrient
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNutrientRequest $request, Nutrient $nutrient)
    {
        $nutrient->update($request->all());

        return redirect()->route('admin.nutrients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nutrient  $nutrient
     * @return \Illuminate\Http\Response
     */
    public function show(Nutrient $nutrient)
    {
        abort_if(Gate::denies('nutrient_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.nutrients.show', compact('nutrient'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Nutrient  $nutrient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nutrient $nutrient)
    {
        abort_if(Gate::denies('nutrient_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nutrient->delete();

        return back();
    }

    /**
     *
     */
    public function massDestroy(MassDestroyNutrientRequest $request)
    {
        Nutrient::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

}
