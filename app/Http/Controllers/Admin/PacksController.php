<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPackRequest;
use App\Http\Requests\StorePackRequest;
use App\Http\Requests\UpdatePackRequest;
use App\Models\Recipe;
use App\Models\Ingredient;
use App\Models\IGroup;
use App\Models\Nutrient;
use App\Models\Pack;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class PacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('pack_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $packs = Pack::all();

        return view('admin.packs.index', compact('packs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('pack_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.packs.create', [
            'recipes' => Recipe::get(),
            'ingredients' => Ingredient::get(),
            'nutrients' => Nutrient::get(),
        ]);
    }

    /**
     *
     */
    public function updatePackInfo (Pack $pack )
    {
        define ("INGREDIENT", 0) ;
        define ("GROUP", 1) ;

        $packNutrientsArray = array();
        $packIngredientsArray = array();

        $tmpGroupIngredientsArray = array();
        $tmpIngredientsArray = array();
        $tmpPackIngredientsArray = array();

        $packWeight = 0;
        $packWeight_real = 0;

        // pack weight = pack product * quantity - will be updated bellow
        foreach ( $pack->recipes()->get() as $recipe ) {
            $packWeight +=  $recipe->weight * $recipe->pivot->amount;
            $packWeight_real += $recipe->weight_real * $recipe->pivot->amount;
        }

        foreach ( $pack->recipes()->get() as $recipe ) {

            //dd ($value['amount']);
            $thisreciperelativeweight = ( $recipe->weight * $recipe->pivot->amount ) / $packWeight ;
            $thisreciperelativeweight_real = ( $recipe->weight_real * $recipe->pivot->amount ) / $packWeight_real  ;

            // nutrients
            $recipeNutrients = $recipe->nutrients();
            foreach ($recipeNutrients->get() as $recipeNutrient){
                if ( array_key_exists ( $recipeNutrient->id, $packNutrientsArray ) ) {
                    // nutrient * peso total / peso do produto
                    $packNutrientsArray[$recipeNutrient->id] += round ( $recipeNutrient->pivot->amount * $thisreciperelativeweight, 3, PHP_ROUND_HALF_UP)  ;
                }
                else {
                    $packNutrientsArray[$recipeNutrient->id] = round ( $recipeNutrient->pivot->amount * $thisreciperelativeweight_real, 3, PHP_ROUND_HALF_UP)  ;
                }
            }
            $pack->nutrients()->sync($this->mapNutrients($packNutrientsArray));

            // ingredients
            $recipeIngredients = $recipe->ingredients();
            foreach ($recipeIngredients->get() as $recipeIngredient){
                if ( array_key_exists ( $recipeIngredient->id, $packIngredientsArray ) ) {
                    $packIngredientsArray[$recipeIngredient->id] += round ( $recipeIngredient->pivot->amount * $thisreciperelativeweight, 3, PHP_ROUND_HALF_UP)  ;
                }
                else {
                    $packIngredientsArray[$recipeIngredient->id] = round ( $recipeIngredient->pivot->amount * $thisreciperelativeweight_real, 3, PHP_ROUND_HALF_UP)  ;
                }
            }
            $pack->ingredients()->sync($this->mapIngredients($packIngredientsArray));
        }

        //dd ($packIngredientsArray) ;
        foreach ( $packIngredientsArray as $key => $amount ) {
            // check if ingredients($key) belongs to a group
            $igroup = Ingredient::where('id', $key)->value('igroup_id');
            //var_dump ($key);
            //var_dump ($igroup[0]);
            if ( $igroup ) {
                // ingredient belong to a group
                // add $amount to amount of the group
                if ( array_key_exists ( $igroup, $tmpGroupIngredientsArray ) ) {
                    $tmpGroupIngredientsArray[$igroup] += $amount;
                } else {
                    $tmpGroupIngredientsArray[$igroup] = $amount;
                }
            } else {
                // ingredient doesn't belong to a group
                // leave $amount to ingredient
                if ( array_key_exists ( $key, $tmpIngredientsArray ) ) {
                    $tmpIngredientsArray[$key] += $amount;
                } else {
                    $tmpIngredientsArray[$key] = $amount;
                }
            }
        }

        foreach ( $tmpIngredientsArray as $key => $value ) {
            $tmpPackIngredientsArray[] = array ('type' => INGREDIENT, 'amount' => $value, 'id' => $key) ;
        }
        //dd ($tmpPackIngredientsArray);
        foreach ( $tmpGroupIngredientsArray as $key => $value ) {
            $tmpPackIngredientsArray[] = array ('type' => GROUP, 'amount' => $value, 'id' => $key) ;
        }
        //dd ($tmpPackIngredientsArray);

        // sort multidimensional array
        $amount = array();
        foreach ($tmpPackIngredientsArray as $key => $row)
        {
            $amount[$key] = $row['amount'];
        }
        array_multisort($amount, SORT_DESC, $tmpPackIngredientsArray);
        //dd ($tmpPackIngredientsArray);

        // sort for php7 - not working
//        usort($tmpGroupIngredientsArray, function ($item1, $item2) {
//            return $item2['amount'] <=> $item1['amount'];
//        });

        /**
         *
         */
        $ingredients_info = "" ;
        // sort array keeping the index association
        //dd ($packIngredientsArray);
        arsort($packIngredientsArray);
        foreach ( $tmpPackIngredientsArray as $value ) {
            if ( $value['type'] == INGREDIENT ) {
                // here we add the ingredient name
                $ingredients_info .= Ingredient::where('id', $value['id'])->value('name') . '('. $value['amount'] . '), ';
            } else {
                // here we add the group name
                $ingredients_info .= IGroup::where('id', $value['id'])->value('name') . '('. $value['amount'] . ')';

                // and all ingredients of the product belonging to that group ($value['id'])
                $ingredients_info .= ' (';
                // lets go through all ingredients for this pack
                foreach ( $packIngredientsArray as $ikey => $ivalue ) {
                        // check if the group of this ingredient is our current group. if yes add it to list
                        $i = Ingredient::where('id', $ikey)->first();
                        //echo  $i->name . "-" . $i->igroup_id . "-" . $value['id'] . "-" . $ivalue['type'] . "<br>";
                        if ( $i->igroup_id == $value['id'] ) {
                            $ingredients_info .= $i->name;
                            $ingredients_info .= ', ';
                        }
                }
                $ingredients_info .= '), ';
            }
        }
        // remove extra comma in group ingredients list and extra comma and extra space at the end (last 2 characters)
        $ingredients_info = substr(str_replace ( ", )", ")", $ingredients_info ), 0, -2) ;

        /**
         *
         */
        $nutritional_info = "";
        $nutritional_info .= "Nutrition Information (average values per 100 g): " ;
        $nutritional_info .= "Energy %d kJ/%d kcal |Fat %.1f g. " ;
        $nutritional_info .= "Of which saturates %.1f g |Carbohydrates %.1f g. " ;
        $nutritional_info .= "Of which sugars %.1f g |Protein %.1f g | Fibre %.1f g " ;
        $nutritional_info .= "Salt %.1f g" ;

        if ( count($packNutrientsArray) == 8 ) {
            $nutritional_info = sprintf ( $nutritional_info,
                                            $packNutrientsArray[2],
                                            $packNutrientsArray[1],
                                            $packNutrientsArray[3],
                                            $packNutrientsArray[4],
                                            $packNutrientsArray[5],
                                            $packNutrientsArray[6],
                                            $packNutrientsArray[7],
                                            $packNutrientsArray[8],
                                            $packNutrientsArray[9]
                                        );
        }


                                     /*
        foreach($pack->nutrients()->get() as $nutrient)
                {{ $nutrient->name }} {{ $nutrient->pivot->amount }}
        endforeach
        */

        // update
        $pack->update ( [   'weight'=> $packWeight,
                            'weight_real'=> $packWeight_real,
                            'ingredients_info' => $ingredients_info,
                            'nutritional_info' => $nutritional_info,
                            'allergenic_info' => '' ] ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackRequest $request)
    {
        $data = $request->validated();

        $pack = Pack::create($data);
        $pack->recipes()->sync($this->mapRecipes($data['recipes']));

        $this->updatePackInfo ( $pack );

        if (request()->hasFile('thumbnail')) {
            $thumbnail = request()->file('thumbnail')->getClientOriginalName();
            // problem -> request id not set
            request()->file('thumbnail')->storeAs('packs/'.$pack->id.'/thumbnails', $thumbnail, '');
            $pack->update(['thumbnail'=> $thumbnail]);
        }

        return redirect()->route('admin.packs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pack  $pack
     * @return \Illuminate\Http\Response
     */
    public function show(Pack $pack)
    {
        abort_if(Gate::denies('pack_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // to update info if ingredents or nutrients were changed
        $this->updatePackInfo ( $pack );

        $pack->load('recipes');
        $pack->load('ingredients');
        $pack->load('nutrients');

        return view('admin.packs.show', compact('pack'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pack  $pack
     * @return \Illuminate\Http\Response
     */
    public function edit(Pack $pack)
    {
        abort_if(Gate::denies('pack_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');


        $pack->load('recipes');
        $recipes = Recipe::get()->map(function($recipe) use ($pack) {
            $recipe->value = data_get($pack->recipes->firstWhere('id', $recipe->id), 'pivot.amount') ?? null;
            return $recipe;
        });


        $pack->load('ingredients');
        $ingredients = Ingredient::get()->map(function($ingredient) use ($pack) {
            $ingredient->value = data_get($pack->ingredients->firstWhere('id', $ingredient->id), 'pivot.amount') ?? null;
            return $ingredient;
        });


        $pack->load('nutrients');
        $nutrients = Nutrient::get()->map(function($nutrient) use ($pack) {
            $nutrient->value = data_get($pack->nutrients->firstWhere('id', $nutrient->id), 'pivot.amount') ?? null;
            return $nutrient;
        });

        return view('admin.packs.edit', [
            'ingredients' => $ingredients,
            'nutrients' => $nutrients,
            'recipes' => $recipes,
            'pack' => $pack,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pack  $pack
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackRequest $request, Pack $pack)
    {
        $data = $request->validated();

        $pack->update($data);
        $pack->recipes()->sync($this->mapRecipes($data['recipes']));

        $this->updatePackInfo ( $pack );

        return redirect()->route('admin.packs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pack  $pack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pack $pack)
    {
        abort_if(Gate::denies('pack_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pack->delete();

        return back();
    }

    /**
     *
     */
    public function massDestroy(MassDestroyPackRequest $request)
    {
        Pack::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    private function mapRecipes($recipes)
    {
        return collect($recipes)->map(function ($i) {
            return ['amount' => $i];
        });
    }

    private function mapIngredients($ingredients)
    {
        return collect($ingredients)->map(function ($i) {
            return ['amount' => $i];
        });
    }

    private function mapNutrients($nutrients)
    {
        return collect($nutrients)->map(function ($i) {
            return ['amount' => $i];
        });
    }
}
