<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\IGroup;
use App\Models\Ingredient;

use App\Http\Requests\MassDestroyIGroupRequest;
use App\Http\Requests\StoreIGroupRequest;
use App\Http\Requests\UpdateIGroupRequest;

use Illuminate\Http\Request;
use Gate;
use Symfony\Component\HttpFoundation\Response;


class IGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('igroup_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $igroups = IGroup::all();

        return view('admin.igroups.index', compact('igroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('igroup_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ingredients = Ingredient::all()->pluck('name', 'id');
        return view('admin.igroups.create', compact('ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIGroupRequest $request)
    {
        $igroup = IGroup::create($request->all());

        if ( isset($request->ingredients) ) {
            foreach ( $request->ingredients as $ingredient ) {
                $i = Ingredient::find($ingredient);
                $i->update(['igroup_id' => $igroup->id]);
            }
        }

        return redirect()->route('admin.igroups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupIngredient  $groupIngredient
     * @return \Illuminate\Http\Response
     */
    public function show(IGroup $igroup)
    {
        abort_if(Gate::denies('igroup_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //dd($igroup->ingredients()->get());
        return view('admin.igroups.show', compact('igroup'));
    }

    /**
     *
     */
    public function edit(IGroup $igroup)
    {
        abort_if(Gate::denies('igroup_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ingredients = Ingredient::all()->pluck('name', 'id');
        $igroup->load('ingredients');

        return view('admin.igroups.edit', compact('igroup','ingredients'));
    }

    /**
     *
     */
    public function update(Request $request, IGroup $igroup)
    {
        $igroup->update($request->all());

        // clean existing relations, if any
        if ( $igroup->ingredients()->exists() )  {
            foreach ( $igroup->ingredients()->get() as $ingredient ) {
                $ingredient->update(['igroup_id' => null]);
            }
        }

        // set new relations, if any
        if ( isset($request->ingredients) ) {
            foreach ( $request->ingredients as $ingredient ) {
                $i = Ingredient::find($ingredient);
                $i->update(['igroup_id' => $igroup->id]);
            }
        }

        return redirect()->route('admin.igroups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IGroup  $igroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(IGroup $igroup)
    {
        abort_if(Gate::denies('igroup_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // before deletinga a ingredient group, we need to clear all ingredient relations if any
        if ( $igroup->ingredients()->exists() )  {
            foreach ( $igroup->ingredients()->get() as $ingredient ) {
                $ingredient->update(['igroup_id' => null]);
            }
        }
        $igroup->delete();

        return back();
    }

    public function massDestroy(MassDestroyIGroupRequest $request)
    {
        IGroup::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
