<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePackRequest;
use App\Http\Requests\UpdatePackRequest;
use App\Http\Resources\Admin\PackResource;
use App\Models\Pack;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PacksApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('pack_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PackResource(Pack::with(['recipes'])->get());
    }

    public function store(StorePackRequest $request)
    {
        $pack = Recipe::create($request->all());
        $pack->recipes()->sync($request->input('recipes', []));

        return (new PackResource($pack))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Recipe $pack)
    {
        abort_if(Gate::denies('recipe_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PackResource($pack->load(['recipes']));
    }

    public function update(UpdateRecipeRequest $request, Recipe $pack)
    {
        $pack->update($request->all());
        $pack->recipes()->sync($request->input('recipes', []));

        return (new PackResource($pack))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Recipe $pack)
    {
        abort_if(Gate::denies('pack_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pack->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
