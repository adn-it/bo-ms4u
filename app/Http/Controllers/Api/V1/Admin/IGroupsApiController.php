<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreIngredientRequest;
use App\Http\Requests\UpdateIngredientRequest;
use App\Http\Resources\Admin\IngredientResource;
use App\Http\Resources\Admin\IGroupResource;
use App\Models\IGroup;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IGroupsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('igroup_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new IGroupResource(IGroup::all());
    }

    public function store(StoreIGroupRequest $request)
    {
        $igroup = IGroup::create($request->all());

        return (new IGroupResource($igroup))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(IGroup $igroup)
    {
        abort_if(Gate::denies('igroup_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new IGroupResource($igroup);
    }

    public function update(UpdateIGroupRequest $request, IGroup $igroup)
    {
        $igroup->update($request->all());

        return (new IGroupResource($igroup))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(IGroup $igroup)
    {
        abort_if(Gate::denies('igroup_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $igroup->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
