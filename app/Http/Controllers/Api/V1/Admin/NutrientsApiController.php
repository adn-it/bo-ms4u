<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNutrientRequest;
use App\Http\Requests\UpdateNutrientRequest;
use App\Http\Resources\Admin\NutrientResource;
use App\Models\Nutrient;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NutrientsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('nutrient_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NutrientResource(Nutrient::all());
    }

    public function store(StoreNutrientRequest $request)
    {
        $nutrient = Nutrient::create($request->all());

        return (new NutrientResource($nutrient))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Nutrient $nutrient)
    {
        abort_if(Gate::denies('nutrient_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NutrientResource($nutrient);
    }

    public function update(UpdateNutrientRequest $request, Nutrient $nutrient)
    {
        $nutrient->update($request->all());

        return (new NutrientResource($nutrient))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Nutrient $nutrient)
    {
        abort_if(Gate::denies('nutrient_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $nutrient->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
