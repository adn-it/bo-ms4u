<table>
    @foreach($nutrients as $nutrient)
        <tr>
            <td><input {{ $nutrient->value ? 'checked' : null }} data-id="{{ $nutrient->id }}" type="checkbox" class="nutrient-enable"></td>
            <td>{{ $nutrient->name }}</td>
            <td><input value="{{ $nutrient->value ?? null }}" {{ $nutrient->value ? null : 'disabled' }} data-id="{{ $nutrient->id }}" name="nutrients[{{ $nutrient->id }}]" type="text" class="nutrient-amount form-control" placeholder="Amount"></td>
        </tr>
    @endforeach
</table>

@section('scripts')
    @parent
    <script>
        $('document').ready(function () {
            $('.nutrient-enable').on('click', function () {
                let id = $(this).attr('data-id')
                let enabled = $(this).is(":checked")
                $('.nutrient-amount[data-id="' + id + '"]').attr('disabled', !enabled)
                $('.nutrient-amount[data-id="' + id + '"]').val(null)
            })
        });
    </script>
@endsection
