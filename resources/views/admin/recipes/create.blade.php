@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h1>{{ trans('global.create') }} {{ trans('cruds.recipe.title_singular') }}</h1>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.recipes.store") }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.recipe.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.name_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="fullname">{{ trans('cruds.recipe.fields.fullname') }}</label>
                <input class="form-control {{ $errors->has('fullname') ? 'is-invalid' : '' }}" type="text" name="fullname" id="fullname" value="{{ old('fullname', '') }}" required>
                @if($errors->has('fullname'))
                    <div class="invalid-feedback">
                        {{ $errors->first('fullname') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.fullname_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="weight">{{ trans('cruds.recipe.fields.weight') }}</label>
                <input class="form-control {{ $errors->has('weight') ? 'is-invalid' : '' }}" type="text" name="weight" id="weight" value="{{ old('weight', '') }}" required>
                @if($errors->has('weight'))
                    <div class="invalid-feedback">
                        {{ $errors->first('weight') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.weight_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="weight_real">{{ trans('cruds.recipe.fields.weight_real') }}</label>
                <input class="form-control {{ $errors->has('weight_real') ? 'is-invalid' : '' }}" type="text" name="weight_real" id="weight_real" value="{{ old('weight_real', '') }}" required>
                @if($errors->has('weight_real'))
                    <div class="invalid-feedback">
                        {{ $errors->first('weight_real') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.weight_real_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="technical_sheet_date">{{ trans('cruds.recipe.fields.technical_sheet_date') }}</label>
                <input class="form-control {{ $errors->has('technical_sheet_date') ? 'is-invalid' : '' }}" type="date" name="technical_sheet_date" id="technical_sheet_date" value="{{ old('technical_sheet_date', '') }}" required>
                @if($errors->has('technical_sheet_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('technical_sheet_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.technical_sheet_date_helper') }}</span>
            </div>
<!--
            <div class="form-group">
                <label class="required" for="ingredients">{{ trans('cruds.recipe.fields.ingredients') }}</label>
                @include('admin.recipes.partials.ingredients')
                @if($errors->has('ingredients'))
                    <div class="invalid-feedback">
                        {{ $errors->first('ingredients') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.ingredients_helper') }}</span>
            </div>
-->
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
