@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <h1>{{ trans('cruds.recipe.fields.editProductNutrient') }}</h1>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.recipes.update", [$recipe->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <input type="hidden" name="name" id="name" value="{{ old('name', $recipe->name) }}">
            <input type="hidden" name="fullname" id="fullname" value="{{ old('fullname', $recipe->fullname) }}">

            <div class="form-group">
                <label class="required" for="nutrients"><h3>{{ trans('cruds.recipe.fields.nutrients') }} for {{ $recipe->name }} / {{ $recipe->fullname }}</h3></label>
                <table class="table-sm table-bordered table-striped">
                    @foreach($nutrients as $nutrient)
                        <tr>
                            <td>{{ $nutrient->name }}</td>
                            <td><input value="{{ $nutrient->value ?? null }}" data-id="{{ $nutrient->id }}" name="nutrients[{{ $nutrient->id }}]" type="text" class="nutrient-amount form-control" placeholder="Amount"></td>
                        </tr>
                    @endforeach
                </table>
                @if($errors->has('nutrients'))
                <div class="invalid-feedback">
                    {{ $errors->first('nutrients') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.nutrients_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="technical_sheet_date">{{ trans('cruds.recipe.fields.technical_sheet_date') }}</label>
                <input class="form-control {{ $errors->has('technical_sheet_date') ? 'is-invalid' : '' }}" type="date" name="technical_sheet_date" id="technical_sheet_date" value="{{ old('technical_sheet_date', $recipe->technical_sheet_date) }}" required>
                @if($errors->has('technical_sheet_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('technical_sheet_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.technical_sheet_date_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
