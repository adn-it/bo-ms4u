@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.recipe.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.recipes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.id') }}
                        </th>
                        <td>
                            {{ $recipe->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.name') }} - {{ trans('cruds.recipe.fields.name') }}
                        </th>
                        <td>
                            {{ $recipe->name }} - {{ $recipe->fullname }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.thumbnail') }}
                        </th>
                        <td>
                            <img src="/storage/products/{{ $recipe->id }}/thumbnails/{{ $recipe->thumbnail }}">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.weight') }} / {{ trans('cruds.recipe.fields.weight_real') }}
                        </th>
                        <td>
                            {{ $recipe->weight }} / {{ $recipe->weight_real }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.technical_sheet_date') }}
                        </th>
                        <td>
                            {{ $recipe->technical_sheet_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.ingredients') }}
                        </th>
                        <td>
                            <table class="table-sm table-bordered table-striped">
                                @foreach($recipe->ingredients as $key => $ingredient)
                                <tr>
                                    <td><div class="label label-info">{{ $ingredient->name }}</div></td><td>{{ $ingredient->pivot->amount }}</td>
                                </tr>
                                @endforeach
                                <tr><td></td><td>{{$totalingredients}}</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.recipe.fields.nutrients') }}
                        </th>
                        <td>
                            <table class="table-sm table-bordered table-striped">
                                @foreach($recipe->nutrients as $key => $nutrient)
                                <tr>
                                    <td><div class="label label-info">{{ $nutrient->name }}</div></td><td>{{ $nutrient->pivot->amount }}</td>
                                </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>

                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.recipes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
