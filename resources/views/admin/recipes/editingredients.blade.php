@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <h1>{{ trans('cruds.recipe.fields.editProductIngredient') }}</h1>
    </div>

    <div class="card-body">
        <div>
            <h2>{{ trans('cruds.recipe.fields.ingredientsweight')}}: {{ $ingredientsweight }}</h2>
            {{ trans('Select ingredients present in this products and enter their weight') }}
            <br><br>
        </div>
        <form method="POST" action="{{ route("admin.recipes.update", [$recipe->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <input type="hidden" name="name" id="name" value="{{ old('name', $recipe->name) }}">
            <input type="hidden" name="fullname" id="fullname" value="{{ old('fullname', $recipe->fullname) }}">

            <div class="form-group">
                <!--<label class="required" for="ingredients">{{ trans('cruds.recipe.fields.ingredients') }}</label>-->

                @include('admin.recipes.partials.ingredients')

                @if($errors->has('ingredients'))
                    <div class="invalid-feedback">
                        {{ $errors->first('ingredients') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.ingredients_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="technical_sheet_date">{{ trans('cruds.recipe.fields.technical_sheet_date') }}</label>
                <input class="form-control {{ $errors->has('technical_sheet_date') ? 'is-invalid' : '' }}" type="date" name="technical_sheet_date" id="technical_sheet_date" value="{{ old('technical_sheet_date', $recipe->technical_sheet_date) }}" required>
                @if($errors->has('technical_sheet_date'))
                    <div class="invalid-feedback">
                        {{ $errors->first('technical_sheet_date') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.recipe.fields.technical_sheet_date_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
