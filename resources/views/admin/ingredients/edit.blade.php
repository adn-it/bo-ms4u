@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.ingredient.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ingredients.update", [$ingredient->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.ingredient.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $ingredient->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ingredient.fields.name_helper') }}</span>
            </div>

            <div class="form-group">
                <label for="igroup">{{ trans('cruds.ingredient.fields.igroup') }}</label>

                <select class="form-control select {{ $errors->has('igroup') ? 'is-invalid' : '' }}" name="igroup_id" id="igroup_id">
                    <option value=""> </option>
                    @foreach($igroups as $id => $igroup)
                        <option value="{{ $id }}" {{ $id == old('igroup', $ingredient->igroup_id) ? 'selected' : '' }}>{{ $igroup }}</option>
                    @endforeach
                </select>
                @if($errors->has('igroup'))
                    <div class="invalid-feedback">
                        {{ $errors->first('igroup') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ingredient.fields.igroup_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
