@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.ingredient.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ingredients.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.ingredient.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ingredient.fields.name_helper') }}</span>
            </div>

            <div class="form-group">
                <label for="igroup">{{ trans('cruds.ingredient.fields.igroup') }}</label>

                <select class="form-control select {{ $errors->has('igroup') ? 'is-invalid' : '' }}" name="igroup_id" id="igroup_id">
                    <option value=""> </option>
                    @foreach($igroups as $id => $igroup)
                        <option value="{{ $id }}" {{ in_array($id, old('igroup', [])) ? 'selected' : '' }}>{{ $igroup }}</option>
                    @endforeach
                </select>
                @if($errors->has('igroup'))
                    <div class="invalid-feedback">
                        {{ $errors->first('igroup') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.ingredient.fields.igroup_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
