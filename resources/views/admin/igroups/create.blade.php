@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.igroup.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.igroups.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.igroup.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.igroup.fields.name_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="" for="ingredients">{{ trans('cruds.igroup.fields.ingredients') }}</label>

                <select class="form-control select2 {{ $errors->has('ingredients') ? 'is-invalid' : '' }}" name="ingredients[]" id="ingredients" multiple >
                    @foreach($ingredients as $id => $ingredient)
                        <option value="{{ $id }}" {{ in_array($id, old('ingredients', [])) ? 'selected' : '' }}>{{ $ingredient }}</option>
                    @endforeach
                </select>
                @if($errors->has('ingredients'))
                    <div class="invalid-feedback">
                        {{ $errors->first('ingredients') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.igroup.fields.ingredients_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
