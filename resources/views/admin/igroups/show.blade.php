@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.igroup.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.igroups.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.igroup.fields.id') }}
                        </th>
                        <td>
                            {{ $igroup->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.igroup.fields.name') }}
                        </th>
                        <td>
                            {{ $igroup->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>{{ trans('cruds.igroup.fields.ingredients') }}</td>
                        <td>
                            @foreach ( $igroup->ingredients()->get() as $ingredient )
                                {{$ingredient->name}}@if(!$loop->last), @endif
                            @endforeach
                        </td>
                    </tr>

                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.igroups.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
