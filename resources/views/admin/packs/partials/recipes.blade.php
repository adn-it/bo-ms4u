<table>
    @foreach($recipes as $recipe)
        <tr>
            <td><input {{ $recipe->value ? 'checked' : null }} data-id="{{ $recipe->id }}" type="checkbox" class="recipe-enable"></td>
            <td>{{ $recipe->name }}</td>
            <td><input value="{{ $recipe->value ?? null }}" {{ $recipe->value ? null : 'disabled' }} data-id="{{ $recipe->id }}" name="recipes[{{ $recipe->id }}]" type="text" class="recipe-amount form-control" placeholder="Amount"></td>
        </tr>
    @endforeach
</table>

@section('scripts')
    @parent
    <script>
        $('document').ready(function () {
            $('.recipe-enable').on('click', function () {
                let id = $(this).attr('data-id')
                let enabled = $(this).is(":checked")
                $('.recipe-amount[data-id="' + id + '"]').attr('disabled', !enabled)
                $('.recipe-amount[data-id="' + id + '"]').val(null)
            })
        });
    </script>
@endsection
