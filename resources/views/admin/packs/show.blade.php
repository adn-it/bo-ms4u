@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.pack.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.packs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.id') }}
                        </th>
                        <td>
                            {{ $pack->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.name') }}
                        </th>
                        <td>
                            {{ $pack->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.fullname') }}
                        </th>
                        <td>
                            {{ $pack->fullname }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.recipes') }}
                        </th>
                        <td>
                            <div class="label label-info">
                            @foreach($pack->recipes as $key => $recipe)
                                {{ $recipe->name }} ({{ $recipe->pivot->amount }})
                            @endforeach
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.ingredients') }}
                        </th>
                        <td>
                            <div class="label label-info">{{ trans('Ingredients')}}:
                                {{ $pack->ingredients_info }}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.pack.fields.nutrients') }}
                        </th>
                        <td>
                            <div class="label label-info">
                                {{ $pack->nutritional_info }}  <!--<button value="copy" onclick="copyToClipboard('copy_asdasd')">Copy!</button>-->
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.packs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
