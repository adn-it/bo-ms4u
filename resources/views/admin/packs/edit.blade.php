@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.pack.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.packs.update", [$pack->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.pack.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $pack->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.pack.fields.name_helper') }}</span>
            </div>

            <label class="required" for="fullname">{{ trans('cruds.pack.fields.fullname') }}</label>
            <input class="form-control {{ $errors->has('fullname') ? 'is-invalid' : '' }}" type="text" name="fullname" id="fullname" value="{{ old('fullname', $pack->fullname) }}" required>
            @if($errors->has('fullname'))
                <div class="invalid-feedback">
                    {{ $errors->first('fullname') }}
                </div>
            @endif
            <span class="help-block">{{ trans('cruds.pack.fields.fullname_helper') }}</span>

            <div class="form-group">
                <label class="required" for="recipes">{{ trans('cruds.pack.fields.recipes') }}</label>

                @include('admin.packs.partials.recipes')

                @if($errors->has('recipes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('recipes') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.pack.fields.recipes_helper') }}</span>
            </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
