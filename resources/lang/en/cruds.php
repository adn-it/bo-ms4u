<?php

return [
    'userManagement' => [
        'title'          => 'User management',
        'title_singular' => 'User management',
    ],
    'permission'     => [
        'title'          => 'Permissions',
        'title_singular' => 'Permission',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'title'             => 'Title',
            'title_helper'      => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'role'           => [
        'title'          => 'Roles',
        'title_singular' => 'Role',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'title'              => 'Title',
            'title_helper'       => ' ',
            'permissions'        => 'Permissions',
            'permissions_helper' => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
        ],
    ],
    'user'           => [
        'title'          => 'Users',
        'title_singular' => 'User',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => ' ',
            'name'                     => 'Name',
            'name_helper'              => ' ',
            'email'                    => 'Email',
            'email_helper'             => ' ',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => ' ',
            'password'                 => 'Password',
            'password_helper'          => ' ',
            'roles'                    => 'Roles',
            'roles_helper'             => ' ',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => ' ',
            'created_at'               => 'Created at',
            'created_at_helper'        => ' ',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => ' ',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => ' ',
        ],
    ],
    'ingredientManagement' => [
        'title'          => 'Ingredients management',
        'title_singular' => 'Ingredient management',
    ],
    'ingredient'     => [
        'title'          => 'Ingredients',
        'title_singular' => 'Ingredient',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Name',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
            'igroup'            => 'Group',
            'igroup_helper'     => 'Select Group for this Ingredient. Leave blank for no group',
        ],
    ],
    'igroup'     => [
        'title'          => 'Groups',
        'title_singular' => 'Group',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Name',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
            'igroup'            => 'Ingredient Group',
            'ingredients'       => 'Ingredients',
            'igroups_helper'    => ' ',
            'ingredients_helper'=> 'Select ingredients for this group',
        ],
    ],
    'nutrient'     => [
        'title'          => 'Nutrients',
        'title_singular' => 'Nutrient',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Name',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'recipe'         => [
        'title'          => 'Products',
        'title_singular' => 'Product',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'name'               => 'Name',
            'name_helper'        => ' ',
            'fullname'           => 'Fullname',
            'fullname_helper'    => ' ',
            'thumbnail'          => 'Thumbnail',
            'thumbnail_helper'   => ' ',
            'weight'             => 'Weight',
            'weight_helper'      => ' ',
            'weight_real'        => 'Real weight',
            'weight_real_helper' => ' ',
            'technical_sheet_date'               => 'Technical Sheet Date',
            'technical_sheet_date_helper'        => ' ',
            'ingredients'        => 'Ingredients',
            'ingredients_helper' => ' ',
            'nutrients'          => 'Nutrients',
            'nutrients_helper'   => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
            'editProductNutrient'=> 'Product Nutrients',
            'editProductIngredient'=> 'Product Ingredients',
        ],
    ],
    'pack'         => [
        'title'          => 'Packs',
        'title_singular' => 'Pack',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'name'               => 'Name',
            'name_helper'        => ' ',
            'recipes'            => 'Products',
            'recipes_helper'     => ' ',
            'fullname'           => 'Fullname',
            'fullname_helper'    => ' ',
            'thumbnail'          => 'Thumbnail',
            'thumbnail_helper'   => ' ',
            'ingredients'        => 'Ingredients',
            'ingredients_helper' => ' ',
            'nutrients'          => 'Nutrients',
            'nutrients_helper'   => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
        ],
    ],
];

